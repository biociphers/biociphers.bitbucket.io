###############
Getting Started
###############

This guide aims to get you using MAJIQ and VOILA productively as quickly as
possible.
It is designed to introduce the main concepts for new users.

.. toctree::
   :maxdepth: 2
   :hidden:

   installing
   quick-overview
   faq
   builder
   statistics
