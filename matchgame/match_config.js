const match_config = [
    {
        id: 'anu',
        name: 'Dr. Anupama (Anu) Jha',
        image: 'img/anu_person.png',
        avatar: 'img/anu_avatar.png',
        bike: 'img/anu_bike.png',
        avatar_desc: 'Hercule Poirot (https://en.wikipedia.org/wiki/Hercule_Poirot)',
        person_desc: 'CIS PhD Student 2014-2020',
        current_desc: 'Postdoc in Bill Noble lab, UW (https://www.linkedin.com/in/anupama-jha-b8825376/)'
    },
    {
        id: 'barry',
        name: 'Barry Slaff',
        image: 'img/barry_person.png',
        avatar: 'img/barry_avatar.png',
        bike: 'img/barry_bike.png',
        avatar_desc: 'Sherlok Holmes, Star Trek (https://en.wikipedia.org/wiki/Elementary,_Dear_Data)',
        person_desc: 'CIS PhD student 2018-2019',
        current_desc: 'Software Development Engineer at Amazon (https://www.linkedin.com/in/barry-slaff-8001b651/)'
    },
    {
        id: 'caleb',
        name: 'Dr. Caleb Radens',
        image: 'img/caleb_person.png',
        avatar: 'img/caleb_avatar.png',
        bike: 'img/caleb_bike.png',
        avatar_desc: 'Fantastic Mr. Fox ( https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film) )',
        person_desc: 'G&E PhD student 2016-2020',
        current_desc: 'Scientific Investigator, Human Genetics Computational Biology at GSK (https://www.linkedin.com/in/calebradens/)'
    },
    {
        id: 'cj',
        name: 'CJ Green',
        image: 'img/cj_person.png',
        avatar: 'img/cj_avatar.png',
        bike: 'img/cj_bike.png',
        avatar_desc: 'Calvin (https://en.wikipedia.org/wiki/Calvin_and_Hobbes)',
        person_desc: 'Web/GUI Developer 2016-2018',
        current_desc: 'Industry'
    },
    {
        id: 'cooldavid',
        name: 'David Wang',
        image: 'img/cooldavid_person.png',
        avatar: 'img/cooldavid_avatar.png',
        bike: 'img/cooldavid_bike.png',
        avatar_desc: 'Super Mario https://en.wikipedia.org/wiki/Super_Mario',
        person_desc: 'GCB PhD Student (2018-)',
        current_desc: 'GCB PhD Student'
    },
    {
        id: 'danielle',
        name: 'Dr. Danielle Gutman',
        image: 'img/danielle_person.png',
        avatar: 'img/danielle_avatar.png',
        bike: 'img/danielle_bike.png',
        avatar_desc: 'Princess Fiona (Shrek) https://en.wikipedia.org/wiki/Fiona_(Shrek)',
        person_desc: 'Postdoctoral researcher (2020-)',
        current_desc: 'Postdoctoral researcher'
    },
    {
        id: 'di',
        name: 'Di Wu',
        image: 'img/di_person.png',
        avatar: 'img/di_avatar.png',
        bike: 'img/di_bike.png',
        avatar_desc: 'Kung Fu Panda https://en.wikipedia.org/wiki/Kung_Fu_Panda',
        person_desc: 'CIS PhD Student (2020-)',
        current_desc: 'CIS PhD Student'
    },
    {
        id: 'farica',
        name: 'Farica Zhuang',
        image: 'img/farica_person.png',
        avatar: 'img/farica_avatar.png',
        bike: 'img/farica_bike.png',
        avatar_desc: 'Stan Marsh (South Park) https://en.wikipedia.org/wiki/Stan_Marsh',
        person_desc: 'CIS PhD Student (2021-)',
        current_desc: 'CIS PhD Student'
    },
    {
        id: 'jordi',
        name: 'Jordi Vaquero',
        image: 'img/jordi_person.png',
        avatar: 'img/jordi_avatar.png',
        bike: 'img/jordi_bike.png',
        avatar_desc: 'Oscar the Grouch (Sesame Street) https://en.wikipedia.org/wiki/Oscar_the_Grouch',
        person_desc: 'Senior Developer and Bioinformatician (2012-2019)',
        current_desc: 'Senior Software Engineer at Metempsy (https://www.linkedin.com/in/jorge-daniel-vaquero-garcia-1b0940a/)'
    },
    {
        id: 'joseph',
        name: 'Dr. Joseph Aicher',
        image: 'img/joseph_person.png',
        avatar: 'img/joseph_avatar.png',
        bike: 'img/joseph_bike.png',
        avatar_desc: 'Aang (The last airbender) https://en.wikipedia.org/wiki/Aang',
        person_desc: 'MD/PhD GCB Student (2018-2022)',
        current_desc: 'Completing his MD'
    },
    {
        id: 'kevin',
        name: 'Kevin Yang',
        image: 'img/kevin_person.png',
        avatar: 'img/kevin_avatar.png',
        bike: 'img/kevin_bike.png',
        avatar_desc: 'Wanpanman https://en.wikipedia.org/wiki/One-Punch_Man',
        person_desc: 'GCB MD/PhD Student',
        current_desc: 'GCB MD/PhD Student'
    },
    {
        id: 'mathieu',
        name: 'Mathieu Quesnel-Valli�res',
        image: 'img/mathieu_person.png',
        avatar: 'img/mathieu_avatar.png',
        bike: 'img/mathieu_bike.png',
        avatar_desc: 'Lumberjack Hockey mascot',
        person_desc: 'Postdoctoral researcher (2018-)',
        current_desc: 'Postdoctoral researcher (2018-)'
    },
    {
        id: 'matt',
        name: 'Matthew Gazzara',
        image: 'img/matt_person.png',
        avatar: 'img/matt_avatar.png',
        bike: 'img/matt_bike.png',
        avatar_desc: 'Eddard "Ned" Stark (Game of Thrones)',
        person_desc: 'Research Staff (2013-2017), GCB PhD Student (2018-)',
        current_desc: 'GCB PhD Student (2018-)'
    },
    {
        id: 'moein',
        name: 'Moein Elzubeir',
        image: 'img/moein_person.png',
        avatar: 'img/moein_avatar.png',
        bike: 'img/moein_bike.png',
        avatar_desc: 'Riley Freeman (The Boondocks) https://en.wikipedia.org/wiki/Riley_Freeman',
        person_desc: 'APSA undergrad intern (2020-2021)',
        current_desc: 'CS undergraduate Columbia University https://www.linkedin.com/in/moein-elzubeir/'
    },
    {
        id: 'osvaldo',
        name: 'Dr. Osvaldo Rivera',
        image: 'img/osvaldo_person.png',
        avatar: 'img/osvaldo_avatar.png',
        bike: 'img/osvaldo_bike.png',
        avatar_desc: 'Son Goku (Dragon ball) https://dragonball.fandom.com/wiki/Goku',
        person_desc: 'Cancer Biology PhD student (2017-2021)',
        current_desc: 'Bioinformatics Scientist, EclipseBio https://www.linkedin.com/in/osvaldoriveraphd/'
    },
    {
        id: 'prasoon',
        name: 'Prasoon Joshi',
        image: 'img/prasoon_person.png',
        avatar: 'img/prasoon_avatar.png',
        bike: 'img/prasoon_bike.png',
        avatar_desc: 'Phineas (Phineas and Ferb) https://en.wikipedia.org/wiki/Phineas_and_Ferb',
        person_desc: 'Summer Intern (remote)',
        current_desc: 'Undergraduate, Indian Institute of Technology (IIT), Kharagpur'
    },
    {
        id: 'scott',
        name: 'Dr. Scott Norton',
        image: 'img/scott_person.png',
        avatar: 'img/scott_avatar.png',
        bike: 'img/scott_bike.png',
        avatar_desc: 'Professor Farnsworth (Futurama) https://en.wikipedia.org/wiki/Professor_Farnsworth',
        person_desc: 'GCB PhD Student (2015-2019)',
        current_desc: 'Senior Bioinfromatics Scientist, Merk https://www.linkedin.com/in/scott-norton-phd/'
    },
    {
        id: 'seong',
        name: 'Seong Han',
        image: 'img/seong_person.png',
        avatar: 'img/seong_avatar.png',
        bike: 'img/seong_bike.png',
        avatar_desc: 'Monkey Luffy (One Piece) https://en.wikipedia.org/wiki/Monkey_D._Luffy',
        person_desc: 'CIS PhD student (2022-)',
        current_desc: 'CIS PhD student (2022-)'
    },
    {
        id: 'uncooldavid',
        name: 'Dr. David Lee',
        image: 'img/uncooldavid_person.png',
        avatar: 'img/uncooldavid_avatar.png',
        bike: 'img/uncooldavid_bike.png',
        avatar_desc: 'Evil Morty (Rick and Morty TV show) https://villains.fandom.com/wiki/Evil_Morty',
        person_desc: 'MD/PhD GCB Student (2018-2021)',
        current_desc: 'Postdoc at the Biociphers lab (2022)'
    },
    {
        id: 'viviana',
        name: 'Viviana Perry',
        image: 'img/viviana_person.png',
        avatar: 'img/viviana_avatar.png',
        bike: 'img/viviana_bike.png',
        avatar_desc: 'Prison Mike (The Office)',
        person_desc: 'APSA undergrad intern (2020-2021)',
        current_desc: 'Neuroscience undergraduate, University of Texas at Austin'
    },
    {
        id: 'yoseph',
        name: 'Yoseph Barash',
        image: 'img/yoseph_person.png',
        avatar: 'img/yoseph_avatar.png',
        bike: 'img/yoseph_bike.png',
        avatar_desc: 'Iron Fist (the original, not the modern Netflix rendition)',
        person_desc: 'Associate Professor (Genetics and CIS)',
        current_desc: 'PI, BioCiphers lab, UPenn'
    },
    {
        id: 'isaac',
        name: 'Isaac Hoskins',
        image: 'img/isaac_person.png',
        avatar: 'img/isaac_avatar.png',
        bike: 'img/isaac_bike.png',
        avatar_desc: '',
        person_desc: '',
        current_desc: ''
    },
    {
        id: 'san',
        name: 'San Jewell',
        image: 'img/san_person.png',
        avatar: 'img/san_avatar.png',
        bike: 'img/san_bike.png',
        avatar_desc: '𓇋𓈖𓊌𓃢𓏏𓐎 (Anput, Female counterpart to Anubis, ancient Egyptian god of death, here depicted in the Marval comic "Moon Knight" ',
        person_desc: 'Full stack developer (and other technical curiosities)',
        current_desc: 'Upenn'
    }
]